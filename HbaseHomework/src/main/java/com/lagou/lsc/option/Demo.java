package com.lagou.lsc.option;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.*;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;


public class Demo {
    Configuration conf = null;
    Connection conn = null;
    HBaseAdmin admin = null;

    @Before
    public void init() throws IOException {
        //初始化链接，设置参数
        conf = HBaseConfiguration.create();
        conf.set("hbase.zookeeper.quorum","192.168.247.129,192.168.247.130");
        conf.set("hbase.zookeeper.property.clientPort","2181");
        conn= ConnectionFactory.createConnection(conf);

    }

    @Test
    public void createtable() throws IOException {
        // admin = new HBaseAdmin(conn);
        admin = (HBaseAdmin) conn.getAdmin();
        //创建表描述器
        HTableDescriptor relation = new HTableDescriptor(Bytes.toBytes("relation"));
        //创建列族
        relation.addFamily(new HColumnDescriptor("friends"));
        //执行创建操作
        admin.createTable(relation);
        System.out.println("---------表创建完成-----------");
    }

    @Test
    public void updata() throws IOException {
        //获取表对象
        Table relation = conn.getTable(TableName.valueOf("relation"));
        ArrayList<Put> list = new ArrayList<Put>();
        //准备put对象，设定rowkey
        Put uid1 = new Put(Bytes.toBytes("uid1"));
        Put uid2 = new Put(Bytes.toBytes("uid2"));
        Put uid3 = new Put(Bytes.toBytes("uid3"));
        Put uid4 = new Put(Bytes.toBytes("uid4"));
        //列族、列、value
        uid1.addColumn(Bytes.toBytes("friends"),Bytes.toBytes("uid2"),Bytes.toBytes("uid"));
        uid1.addColumn(Bytes.toBytes("friends"),Bytes.toBytes("uid3"),Bytes.toBytes("uid"));
        uid1.addColumn(Bytes.toBytes("friends"),Bytes.toBytes("uid4"),Bytes.toBytes("uid"));
        list.add(uid1);
        uid2.addColumn(Bytes.toBytes("friends"),Bytes.toBytes("uid1"),Bytes.toBytes("uid"));
        uid2.addColumn(Bytes.toBytes("friends"),Bytes.toBytes("uid3"),Bytes.toBytes("uid"));
        uid2.addColumn(Bytes.toBytes("friends"),Bytes.toBytes("uid4"),Bytes.toBytes("uid"));
        list.add(uid2);
        uid3.addColumn(Bytes.toBytes("friends"),Bytes.toBytes("uid1"),Bytes.toBytes("uid"));
        uid3.addColumn(Bytes.toBytes("friends"),Bytes.toBytes("uid2"),Bytes.toBytes("uid"));
        uid3.addColumn(Bytes.toBytes("friends"),Bytes.toBytes("uid4"),Bytes.toBytes("uid"));
        list.add(uid3);
        uid4.addColumn(Bytes.toBytes("friends"),Bytes.toBytes("uid1"),Bytes.toBytes("uid"));
        uid4.addColumn(Bytes.toBytes("friends"),Bytes.toBytes("uid2"),Bytes.toBytes("uid"));
        uid4.addColumn(Bytes.toBytes("friends"),Bytes.toBytes("uid3"),Bytes.toBytes("uid"));
        list.add(uid4);

        //添加协处理器之后
        /*uid1.addColumn(Bytes.toBytes("friends"),Bytes.toBytes("uid2"),Bytes.toBytes("uid"));
        uid1.addColumn(Bytes.toBytes("friends"),Bytes.toBytes("uid3"),Bytes.toBytes("uid"));
        uid1.addColumn(Bytes.toBytes("friends"),Bytes.toBytes("uid4"),Bytes.toBytes("uid"));
        list.add(uid1);
        uid2.addColumn(Bytes.toBytes("friends"),Bytes.toBytes("uid3"),Bytes.toBytes("uid"));
        uid2.addColumn(Bytes.toBytes("friends"),Bytes.toBytes("uid4"),Bytes.toBytes("uid"));
        list.add(uid2);
        uid3.addColumn(Bytes.toBytes("friends"),Bytes.toBytes("uid4"),Bytes.toBytes("uid"));
        list.add(uid3);
        list.add(uid4);*/

        relation.put(list);
        relation.close();
        System.out.println("----------插入完成---------");
    }

    @Test
    public void deletedata() throws IOException {
        //获取表对象
        Table relation = conn.getTable(TableName.valueOf("relation"));
        //准备delete对象
        Delete delete = new Delete(Bytes.toBytes("uid1"));
        delete.addColumn(Bytes.toBytes("friends"),Bytes.toBytes("uid2"));
        //执行
        relation.delete(delete);
        relation.close();
        System.out.println("---------------删除完成---------------");
    }

    @Test
    //全表扫描
    public void scan() throws IOException {
        //获取表对象
        Table worker = conn.getTable(TableName.valueOf("relation"));
        //准备scan对象
        Scan scan = new Scan();
        //执行
        ResultScanner scanner = worker.getScanner(scan);
        for (Result result : scanner) {
            //获取result中所有的cell对象
            Cell[] cells = result.rawCells();
            //遍历打印
            for (Cell cell : cells) {
                String rowkey = Bytes.toString(CellUtil.cloneRow(cell));
                String f = Bytes.toString(CellUtil.cloneFamily(cell));
                String q = Bytes.toString(CellUtil.cloneQualifier(cell));
                String value = Bytes.toString(CellUtil.cloneValue(cell));
                System.out.println("rowkey:"+rowkey+" | "+
                        "Family:"+f+" | "+
                        "Qualifier:"+q+" | "+
                        "Value:"+value+"\n"+
                        "-------------------------------------------------------------------");
            }
        }
        worker.close();
    }

    @After
    public void destroy(){
        if(admin!=null) {
            try {
                admin.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if(conn!=null) {
            try {
                conn.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
