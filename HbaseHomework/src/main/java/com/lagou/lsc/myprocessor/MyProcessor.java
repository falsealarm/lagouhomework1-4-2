package com.lagou.lsc.myprocessor;

import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.coprocessor.BaseRegionObserver;
import org.apache.hadoop.hbase.coprocessor.ObserverContext;
import org.apache.hadoop.hbase.coprocessor.RegionCoprocessorEnvironment;
import org.apache.hadoop.hbase.regionserver.wal.WALEdit;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.util.List;

public class MyProcessor extends BaseRegionObserver {
    /*@Override
    public void prePut(ObserverContext<RegionCoprocessorEnvironment> e, Put put, WALEdit edit, Durability durability) throws IOException {
        HTable relation = (HTable) e.getEnvironment().getTable(TableName.valueOf("relation"));

        List<Cell> friends = put.getFamilyCellMap().get(Bytes.toBytes("friends"));

        for (Cell friend : friends) {
            //byte[] family = CellUtil.cloneFamily(friend);
            byte[] qualifier = CellUtil.cloneQualifier(friend);
            byte[] value = CellUtil.cloneValue(friend);
            Put put1 = new Put(qualifier);
            put1.addColumn(Bytes.toBytes("friends"),put.getRow(),value);
            relation.put(put1);
        }
        relation.close();
    }*/
    @Override
    public void postDelete(ObserverContext<RegionCoprocessorEnvironment> e, Delete delete, WALEdit edit, Durability durability) throws IOException {
        //HTable relation = (HTable) e.getEnvironment().getTable(TableName.valueOf("relation"));
        //与视频中不同，必须使用HTableInterface类型，Htable类型无效
        HTableInterface relation = e.getEnvironment().getTable(TableName.valueOf("relation"));
        List<Cell> friends = delete.getFamilyCellMap().get(Bytes.toBytes("friends"));
        if(friends.isEmpty()){
            relation.close();
            return;
        }

        for (Cell friend : friends) {
            //byte[] family = CellUtil.cloneFamily(friend);
            byte[] qualifier = CellUtil.cloneQualifier(friend);
            Delete delete1 = new Delete(qualifier);
            delete1.addColumns(Bytes.toBytes("friends"),CellUtil.cloneRow(friend));
            relation.delete(delete1);
        }
        relation.close();
    }
}
